import os
import json

PATH_FILES = '/Users/miguelgemio/PycharmProjects/demo2/test_files'


def search_files(path, extension='json'):
    list_path_files = []
    for root, dirs, files in os.walk(path):
        files_full_name = [os.path.join(root, f) for f in files if f.endswith(extension)]
        list_path_files.extend(files_full_name)
    return list_path_files


def read_data(files=[]):
    list_files_data = []
    for path_file in files:
        try:
            with open(path_file, 'r') as file:
                list_files_data.append(json.load(file))
        except Exception as e:
            print('Something happened', e)
        finally:
            return list_files_data


if __name__ == '__main__':
    print(search_files(PATH_FILES, 'json'))
