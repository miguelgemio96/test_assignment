def write_txt_file(data, output_path):
    file = open(output_path, 'w+')
    if data:
        for line in data:
            file.write(line+'\n')
    else:
        file.write('')
    file.close()

    answer = {}
    try:
        with open(output_path, 'r') as new_file:
            new_file.read()
        answer = {'statusCode': 'ok', 'body': None}
    except Exception as e:
        print(e)
        answer = {'statusCode': 'error', 'body': None}
    finally:
        return answer


if __name__ == '__main__':
    PATH_PROJECT = '/Users/miguelgemio/PycharmProjects/demo2'

    # Arrange
    filenames = ['data_page_09012022000000.json', 'data_page_09302022000000.json']
    data = [f'{f} - ok' for f in filenames]
    file_sufix = '09302022150001'
    output_path = f'{PATH_PROJECT}/output_{file_sufix}.txt'
    expected_output = {'statusCode': 'ok', 'body': None}

    # Act
    output = write_txt_file(data, output_path)
