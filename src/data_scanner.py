PATH_FILES, EXTENSION = '/Users/miguelgemio/PycharmProjects/demo2/test_files', 'json'


def get_result_keys(item={}):
    key_values = ['serial_no', 'asset_type', 'hardware_standard', 'technical_specs', 'asset_status']
    result = {}
    for key, value in item.items():
        if key in key_values:
            result[key] = value
    return result


def search_asset_by_status(list_items=[], asset_status=''):
    result = []
    if asset_status:
        for item in list_items:
            if item.get('asset_status') == asset_status:
                result.append(item)
    else:
        return list_items
    return result


def search_asset_by_technical_specs(list_items=[], search_tokens=('', '')):
    result = []
    for item in list_items:
        specs = item.get('technical_specs').lower().split(' ')
        first_token, second_token = search_tokens
        if first_token.lower() in specs and second_token.lower() in specs:
            result.append(get_result_keys(item))
    return result


if __name__ == '__main__':
    items = [{
        "id": 2,
        "serial_no": "C02ZL5NRMD6Q",
        "asset_type": "Computer",
        "hardware_standard": "Apple - MacBook Pro (16-inch, 2019)",
        "technical_specs": "8-Core Intel Core i9 / 16GB / 1024GB",
        "asset_status": "Pending Return",
        "imei": "",
        "user": "",
        "employee_id": "",
        "email": "",
        "location": "",
        "network_code": "",
        "lease_start_date": "",
        "lease_end_date": "",
        "loaner_return_date": "",
        "loaner_retention_date": "",
        "carrier": "",
        "child_asset": [],
        "linked_date": "",
        "lost_date": "",
        "cancelled_date": "",
        "end_of_life_date": "",
        "wipe_confirmation": "",
        "donation_certificate": "",
        "age": "0 Year 6 Months",
        "mac_address": "",
        "cost": "",
        "asset_deprecated_value": "",
        "host_name": "",
        "manufacturer_support_end_date": "",
        "modified_date": "03/16/2022",
        "modified_by": None,
        "created_at": "09/29/2021",
        "created_by": "",
        "depreciation": 0
    }]
    print(search_asset_by_technical_specs(items, ('intel', '1024gb')))
